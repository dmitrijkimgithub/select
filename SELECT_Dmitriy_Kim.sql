/*Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?*/
WITH Revenue AS (
SELECT 
s.store_id,
s.staff_id,
SUM(p.amount) AS revenue
FROM
payment p
JOIN rental r ON p.rental_id = r.rental_id
JOIN staff s ON p.staff_id = s.staff_id
WHERE
EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
s.store_id, s.staff_id
),
RankedRevenue AS (
SELECT *,
ROW_NUMBER() OVER(PARTITION BY store_id ORDER BY revenue DESC) RANK 
FROM 
Revenue
)
SELECT
store_id,
staff_id,
revenue
FROM
RankedRevenue
WHERE 
RANK = 1;

SELECT 
payment.staff_id,
store.store_id,
SUM(payment.amount) AS total_revenue
FROM
payment
JOIN rental ON payment.rental_id = rental.rental_id 
JOIN inventory ON rental.inventory_id = inventory.inventory_id 
JOIN store ON inventory.store_id = store.store_id
JOIN staff ON payment.staff_id = staff.staff_id 
WHERE 
DATE_PART('year', rental_date) = 2017
GROUP BY 
payment.staff_id,
store.store_id
ORDER BY 
total_revenue DESC,
store.store_id;

/* Which five movies were rented more than the others, and what is the expected age of the audience for these movies? */
SELECT
film.film_id,
film.title,
count_rentals.total_rentals,
film.rating 
FROM (
SELECT
inventory.film_id,
COUNT(*) AS total_rentals
FROM 
inventory
JOIN rental ON inventory.inventory_id = rental.inventory_id 
GROUP BY 
inventory.film_id
) AS count_rentals
JOIN film ON film.film_id = count_rentals.film_id
ORDER BY 
count_rentals.total_rentals DESC 
LIMIT 5;

SELECT 
f.film_id,
f.title,
f.rating,
COUNT(*) AS rental_count
FROM 
film f
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
GROUP BY 
f.film_id, f.title, f.rating
ORDER BY 
rental_count DESC
LIMIT 5;

/* Which actors/actresses didn't act for a longer period of time than the others? */
SELECT 
a.actor_id,
a.first_name,
a.last_name,
f.release_year AS last_movie_year
FROM
actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id 
JOIN film f ON fa.film_id = f.film_id 
WHERE 
f.release_year = (SELECT MAX(release_year)
FROM
film
WHERE
film_id = fa.film_id)
ORDER BY 
last_movie_year;

SELECT 
a.actor_id,
a.first_name,
a.last_name,
EXTRACT(YEAR FROM current_date) - MAX(f.release_year) AS years_without_acting
FROM 
actor a
LEFT JOIN film_actor fa ON a.actor_id = fa.actor_id
LEFT JOIN (
SELECT 
fa.actor_id,
f.release_year
FROM 
film_actor fa
INNER JOIN film f ON fa.film_id = f.film_id
) f ON a.actor_id = f.actor_id
GROUP BY 
a.actor_id, 
a.first_name, 
a.last_name
ORDER BY 
years_without_acting DESC